#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <mpi.h>
#include "project3.h"

#define ITERATIONS 10000

double When()
{
  struct timeval tp;
  gettimeofday(&tp, NULL);
  return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}

main(int argc, char *argv[])
{
        int iproc, nproc,i, iter;
        char host[255], message[55];
        MPI_Status status;

        MPI_Init(&argc, &argv);
        MPI_Comm_size(MPI_COMM_WORLD, &nproc);
        MPI_Comm_rank(MPI_COMM_WORLD, &iproc);

//        gethostname(host,253);
//        printf("I am proc %d of %d running on %s\n", iproc, nproc,host);

	int numproc = nproc;

	// Get the number of dimensions from nproc
	int numdim = 0;
	while(nproc >>= 1) 
		++numdim;

        // each process has an array of VECSIZE double: ain[VECSIZE]
        float in[VECSIZE];
        
        int myrank, root = 0;
	float maxValue;
        MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
        // Start time here
        srand(myrank+5);
        double start = When();
        for(iter = 0; iter < ITERATIONS; iter++) {
          for(i = 0; i < VECSIZE; i++) {
            in[i] = rand();
//          printf("init proc %d [%d]=%f\n",myrank,i,ain[i]);
          }
         
          maxValue = ReduceVecMax(in, numdim, myrank);
          // At this point, the answer resides on process root
          if (myrank == root) {
              
	      // Read the max value out
//            printf("Root: Max Value = %f\n", maxValue);
              
          }
          // Now broadcast the root vector to everyone else.
          Bcast(in, numdim, myrank);  
        }

        MPI_Finalize();

	// End Time here
        double end = When();
        if(myrank == root) {
          printf("%d, %f\n", numproc, end - start);
        }
}
