#include <math.h>
#include <mpi.h>
#include "project3.h"

// Broadcast value to all nodes
void Bcast(void* buffer, int numdim, int rank)
{
    int notparticipating = pow(2,numdim-1)-1, curdim;
    int bitmask = pow(2,numdim-1);
    MPI_Status status;

    float* currentvalue = (float*)buffer;
    float newvalue[VECSIZE];
    
    for(curdim = 0; curdim < numdim; curdim++) {
	if ((rank & notparticipating) == 0) {
	    if ((rank & bitmask) == 0) {
		int msg_dest = rank ^ bitmask;
		MPI_Send(buffer, VECSIZE, MPI_FLOAT, msg_dest, 0, MPI_COMM_WORLD);
	    } else {
		int msg_src = rank ^ bitmask;
		MPI_Recv(newvalue, VECSIZE, MPI_FLOAT, msg_src, 0, MPI_COMM_WORLD, &status);
            }
        }
	notparticipating >>= 1;
        bitmask >>=1;
    }
    currentvalue = newvalue;
}

// Reduce values to one node
float ReduceVecMax(void* in, int numdim, int rank)
{
    int i, curdim, bitmask = 1, notparticipating = 0;
    float max = ((float*)in)[0];
    MPI_Status status;
 
    for(i = 1; i < VECSIZE; i++)
	max = fmax(max, ((float*)in)[i]);

    float newvalue;
    for(curdim = 0; curdim < numdim; curdim++) {
	if ((rank & notparticipating) == 0) {
	    if ((rank & bitmask) != 0) {
		int msg_dest = rank ^ bitmask;
		MPI_Send(&max, 1, MPI_FLOAT,  msg_dest, 0, MPI_COMM_WORLD);
	    } else {
		int msg_src = rank ^ bitmask;
		MPI_Recv(&newvalue, 1, MPI_FLOAT, msg_src, 0, MPI_COMM_WORLD, &status);
                max = fmax(max, newvalue);
            }
        }
	notparticipating = notparticipating ^ bitmask;
        bitmask <<=1;
    }
    return(max);
}
