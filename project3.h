#ifndef PROJECT3_H_
#define PROJECT3_H_

#define VECSIZE 65536 
void Bcast(void* buffer, int numdim, int rank);
float ReduceVecMax(void* in, int numdim, int rank); 

#endif
