#MPI_HOME        = /users/faculty/snell/mpich
#MPI_INCL        = $(MPI_HOME)/include
#MPI_LIB         = $(MPI_HOME)/lib

#SRC   			= vector.c
#TARGET     		= vector

#CC         		= $(MPI_HOME)/bin/mpicc
#CFLAGS			= -O -I$(MPI_INCL)
#LFLAGS     		= -L$(MPI_LIB) -lm -lmpich

#$(TARGET): $(SRC)
#	$(CC) $(CFLAGS) $(SRC) -o $(TARGET)

#run: $(TARGET)
#	./$(TARGET) -p4pg $(TARGET).cfg

#clean:
#		/bin/rm -f  *.o

MPI_HOME        = /users/faculty/snell/mpich
MPI_INCL        = $(MPI_HOME)/include
MPI_LIB         = $(MPI_HOME)/lib

SRC   			= vector.c project3.c
TARGET     		= vector
NP			= 1

CC         		= $(MPI_HOME)/bin/mpicc
CFLAGS			= -O -I$(MPI_INCL)
LFLAGS     		= -L$(MPI_LIB) -lm -lmpich

$(TARGET): $(SRC)
	$(CC) $(CFLAGS) $(LFLAGS) $(SRC) -o $(TARGET)

benchmark: benchmark.c project3.c project3.h
	$(CC) $(CFLAGS) $(LFLAGS) benchmark.c project3.c -o benchmark

run: $(TARGET)
	mpirun -np $(NP) -machinefile $(TARGET).machines $(TARGET)

clean:
		rm -f  *.o




