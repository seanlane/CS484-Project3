for(( j = 0; j < 5; j++))
do
  for(( i = 0; i <= 5; i++))
  do
    NP=$(awk "BEGIN{print 2 ** $i}")
    mpirun -np $NP -machinefile vector.machines benchmark
  done
done
